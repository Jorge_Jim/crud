<?php
namespace UPT;

class Usuario extends Conexion
{
    public $nombre;
    public $caducidad;
    public $descripcion;
    public $cantidad;
    public $id;
    public function __construct()
    {
        parent::__construct();
    }
    static function vista(){
        $CN = new Conexion();
        $vista = mysqli_prepare($CN->con,"SELECT * FROM productos");
        $vista->execute();
        $resultado = $vista->get_result();
        echo '<style>
            table {
              font-family: Arial, Helvetica, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }
            
            table td, table th {
              border: 1px solid #ddd;
              padding: 8px;
            }
            
            table tr:nth-child(even){background-color: #f2f2f2;}
            
            table tr:hover {background-color: #ddd;}
            
            table th {
              padding-top: 12px;
              padding-bottom: 12px;
              text-align: left;
              background-color: #4CAF50;
              color: white;
            }
            </style>
               <table>
                <tr>
                    <th>Producto</th>
                    <th>Caducidad</th>
                    <th>Descripcion</th>
                    <th>Cantidad</th>
                <tr>
                ';
        while ($fila = mysqli_fetch_assoc($resultado)){
            echo '<tr>
                        <td>'.$fila['nombre'].'</td>
                        <td>'.$fila['caducidad'].'</td>
                        <td>'.$fila['descripcion'].'</td>
                        <td>'.$fila['cantidad'].'</td>
                    <tr>';
        }
        echo "</table>";
    }
    function nuevos(){
        $pre = mysqli_prepare($this->con,"INSERT INTO productos(nombre,caducidad,descripcion,cantidad) VALUES(?,?,?,?)");
        $pre->bind_param("sssi",$this->nombre,$this->caducidad,$this->descripcion,$this->cantidad);
        $pre->execute();
    }
    function actualizar(){
        $actualizo = mysqli_prepare($this->con,"UPDATE productos SET nombre=?, caducidad=?,descripcion=?,cantidad=? WHERE id=?");
        $actualizo->bind_param("ssi",$this->nombre,$this->caducidad,$this->descripcion,$this->cantidad,$this->id);
        $actualizo->execute();
    }
    static function eliminar($id,$nombre){
        $objeto = new Conexion();
        $eliminar = mysqli_prepare($objeto->con,"DELETE FROM productos WHERE id=? AND nombre=?");
        $eliminar->bind_param("is",$id,$nombre);
        $eliminar->execute();
    }
    static function buscar($Finicio,$Ffinal){
        $objeto = new Conexion();
        $busco = mysqli_prepare($objeto->con,"SELECT * FROM productos WHERE caducidad BETWEEN ? AND ?");
        $busco->bind_param("ss",$Finicio,$Ffinal);
        $busco->execute();
        $resultado = $busco->get_result();
        echo '<style>
            table {
              font-family: Arial, Helvetica, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }
            
            table td, table th {
              border: 1px solid #ddd;
              padding: 8px;
            }
            
            table tr:nth-child(even){background-color: #f2f2f2;}
            
            table tr:hover {background-color: #ddd;}
            
            table th {
              padding-top: 12px;
              padding-bottom: 12px;
              text-align: left;
              background-color: #4CAF50;
              color: white;
            }
            </style>
                <h1>Datos buscados</h1>
               <table>
                <tr>
                    <th>Producto</th>
                    <th>Caducidad</th>
                    <th>Descripcion</th>
                    <th>Cantidad</th>
                <tr>
                ';
        while ($fila = mysqli_fetch_assoc($resultado)){
            echo '<tr>
                        <td>'.$fila['nombre'].'</td>
                        <td>'.$fila['caducidad'].'</td>
                        <td>'.$fila['descripcion'].'</td>
                        <td>'.$fila['cantidad'].'</td>
                    <tr>';
        }
        echo "</table>";
    }
}
