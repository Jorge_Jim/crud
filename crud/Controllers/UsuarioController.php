<?php
require 'app/Models/Conexion.php';
require 'app/Models/Usuario.php';

use UPT\Usuario;

class UsuarioController
{

    function inicio(){
        Usuario::vista();
    }
    function nuevo(){
        $nuevo = new Usuario();
        $nuevo->nombre = $_POST['nombre'];
        $nuevo->caducidad = $_POST['caducidad'];
        $nuevo->descripcion = $_POST['descripcion'];
        $nuevo->cantidad = $_POST['cantidad'];
        $nuevo->nuevos();
    }
    function actualizar(){
        $actualizo = new Usuario();
        $actualizo->nombre = $_POST['nombre'];
        $actualizo->caducidad = $_POST['caducidad'];
        $actualizo->descripcion = $_POST['descripcion'];
        $actualizo->cantidad = $_POST['cantidad'];
        $actualizo->id = $_POST['id'];
        $actualizo->actualizar();
    }
    function eliminar(){
        Usuario::eliminar($_POST['id'],$_POST['nombre']);
    }
    function buscar(){
        Usuario::buscar($_POST['FechaInicio'],$_POST['FechaFinal']);
    }
}