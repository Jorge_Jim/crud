<?php
namespace UPT;

class Usuario extends Conexion
{
    public $nombre;
    public $telefono;
    public $id;
    public function __construct()
    {
        parent::__construct();
    }
    static function vista(){
        $CN = new Conexion();
        $vista = mysqli_prepare($CN->con,"SELECT * FROM contactos");
        $vista->execute();
        $resultado = $vista->get_result();
        echo '<style>
            table {
              font-family: Arial, Helvetica, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }
            
            table td, table th {
              border: 1px solid #ddd;
              padding: 8px;
            }
            
            table tr:nth-child(even){background-color: #f2f2f2;}
            
            table tr:hover {background-color: #ddd;}
            
            table th {
              padding-top: 12px;
              padding-bottom: 12px;
              text-align: left;
              background-color: #4CAF50;
              color: white;
            }
            </style>
               <table>
                <tr>
                    <th>Nombre</th>
                    <th>Telefono</th>
                <tr>
                ';
        while ($fila = mysqli_fetch_assoc($resultado)){
            echo '<tr>
                        <td>'.$fila['nombre'].'</td>
                        <td>'.$fila['telefono'].'</td>
                    <tr>';
        }
        echo "</table>";
    }
    function nuevos(){
        $pre = mysqli_prepare($this->con,"INSERT INTO contactos(nombre,telefono) VALUES(?,?)");
        $pre->bind_param("ss",$this->nombre,$this->telefono);
        $pre->execute();
    }
    function actualizar(){
        $actualizo = mysqli_prepare($this->con,"UPDATE contactos SET nombre=?, telefono=? WHERE id=?");
        $actualizo->bind_param("ssi",$this->nombre,$this->telefono,$this->id);
        $actualizo->execute();
    }
    static function eliminar($id,$nombre){
        $objeto = new Conexion();
        $eliminar = mysqli_prepare($objeto->con,"DELETE FROM contactos WHERE id=? AND nombre=?");
        $eliminar->bind_param("is",$id,$nombre);
        $eliminar->execute();
    }
}
