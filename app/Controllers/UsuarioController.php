<?php
require 'app/Models/Conexion.php';
require 'app/Models/Usuario.php';

use UPT\Usuario;

class UsuarioController
{

    function inicio(){
        Usuario::vista();
    }
    function nuevo(){
        $nuevo = new Usuario();
        $nuevo->nombre = $_POST['nombre'];
        $nuevo->telefono = $_POST['telefono'];
        $nuevo->nuevos();
    }
    function actualizar(){
        $actualizo = new Usuario();
        $actualizo->nombre = $_POST['nombre'];
        $actualizo->telefono = $_POST['telefono'];
        $actualizo->id = $_POST['id'];
        $actualizo->actualizar();
    }
    function eliminar(){
        Usuario::eliminar($_POST['id'],$_POST['nombre']);
    }
}